# CS732 Lab 03
**Note:** When running `npm install` to install the dependencies for the frontend, run it with the additional tag `--legacy-peer-deps` so that dependencies resolve correctly:

```sh
npm install --legacy-peer-deps
```

The project uses the following packages we have not covered in class:

- [dummy-json](https://www.npmjs.com/package/dummy-json) (backend)
- [multer](https://www.npmjs.com/package/multer) (backend)
- [mui-rte](https://github.com/niuware/mui-rte) (which is based on [draft-js](https://draftjs.org/)) (frontend)

## Exercise One - Understanding
Build and run the example application, and examine the source code. You'll see that it is a modified version of the "articles viewer" example you've seen previously in the course.

Make some notes about how the application works. In particlar, take note of the differences between this application and the previous examples you've seen. Answer the following questions (in your notes or by editing this README):

- How are all the random articles generated?

- How does the backend accept image uploads when creating new articles? What is `multipart/form-data`? How does the backend let the frontend know where an uploaded image has been stored?

- How does the frontend upload images to the backend? What additional configuration of `axios` is required for this? Why?

- Assuming the frontend and backend are both running (i.e. we are "online"), what is the current behaviour when we add an article? Why?

- Build the frontend, and run the backend in production mode. What is the current functionality of the app when running offline? Why?


## Exercise Two - Better behaviour when adding articles
In Exercise One, you will have noticed that when a new article is added, we are redirected to the correct page that should contain the new article - but we receive an "article doesn't exist" message until we refresh the page.

One way to fix this error would be to force the app to re-fetch the article list whenever we add a new article. Another way would be to modify the code such that we add the new article to our client-side articles list at the same time we send it to the server. What are some benefits and drawbacks of each approach?

For this exercise, we will implement the first approach. Modify the `useGet()` custom hook such that it returns a function called `reFetch()` as part of its return value. When called, that function should force a re-render of the calling component, and when this re-render occurs, the effect function within (that fetches data from the server) should be re-run.

One way this can be implemented is to maintain a stateful value inside `useGet()` which you'll change whenever `reFetch()` is called (this will force a re-render). That value should also be part of the array supplied to `useEffect()` which will be checked for differences whenever a re-render occurs (if the stateful value is different, the effect function will run again).

Once you've implemented `reFetch()`, call it in an appropriate place whenever an article is added, such that the articles list will be re-fetched after the new article is added (the re-fetched list should include the most recently added article).


## Exercise Three - Caching articles
In Exercise One, when you built the frontend for production and ran the server in production mode, you would have noticed that, when running in offline mode, while the application shell loads successfully, the articles will not load if the page is refreshed once offline. This is because there is no caching set up for the articles.

For this exercise, add a new entry to the app's service worker such that it will cache all articles. When implementing this exercise, a *network first* strategy is best - we always want to get the latest articles, unless there is no network connectivity. Only then should the cached articles be used.


## Exercise Four - Placeholder images
Once you've implemented this functionality, the articles should be available offline. However, any articles whose images have not yet been cached, will not have their images shown. Rather than have the browser's default ugly "image load error" graphic, a placeholder image might be a better solution.

To implement this, investigate how to manually precache files, and how to provide fallback responses from your service worker within Workbox (the [workbox documentation](https://developers.google.com/web/tools/workbox/guides/advanced-recipes) will be of great help here). Using your newfound knowledge, you can precache the placeholder image so it will always be available, and then serve it up when any image request fails.

A placeholder image is included in the frontend's [/public/images](./frontend/public/images) directory - or you can use your own.


## Exercise Five - Detecting online status
Let's now let the user know whether they're online or offline, so they can more easily get an immediate visual indication of the kind of service they'll get from the app in the current state.

You'll notice in the page footer that there's an indication of online status - but this is currently hardcoded to always show "online". For this exercise, implement online / offline detection to dynamically update this display whenever connectivity changes. Be sure to design your solution in such a way that it can be reused elsewhere in your code (e.g. a custom hook).

**Hints:**

- You'll find existing custom hooks and other similar solutions online for this. Feel free to use one of those rather than writing your own from scratch.

- You can test your functionality by toggling offline mode in the "Network" tab of your browser's dev tools.
